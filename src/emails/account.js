const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const welcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: process.env.EMAIL,
        subject: 'Welcome',
        text:  `Welcome to the app, ${name}, Let us know how the app goes`
    })
}

const leaveEmail = (email,name) => {
    sgMail.send({
        to: email,
        from: process.env.EMAIL,
        subject: 'GoodBye!!',
        text:  `Goodbye ${name}, We are sorry to see you leaving us. Please let me know if we could of made things better `
    })
}
module.exports = {
    welcomeEmail,
    leaveEmail
}