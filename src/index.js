const express = require('express')
require('./db/mongoose')

const userRtouter = require('./routers/userRouter')
const taskRouter = require('./routers/tasksRouter')

const app = express()
const port = process.env.PORT

app.use(express.json())
app.use(userRtouter)
app.use(taskRouter)




app.listen(port, () => {
    console.log('Server is up on port ' + port)
})
